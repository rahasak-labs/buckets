# buckets

## minio object storage scala client

- read more from [here](https://medium.com/@itseranga/orchestrate-repairs-with-cassandra-reaper-26094bdb59f6).


## swift object storage scala client

- read more from [here](https://medium.com/@itseranga/openstack-swift-scala-client-cee2bfd5258e).


## minio scala client with aws java sdk

- read more from [here](https://medium.com/rahasak/minio-scala-client-with-aws-java-sdk-414de6681527).
